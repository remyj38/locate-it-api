<?php

require_once "./vendor/autoload.php";
use GeoIp2\Database\Reader;

try{
  $reader = new Reader("./database/GeoLite2-City.mmdb");
  $record = $reader->city(htmlspecialchars($_GET["ip"]));
  $return = array(
    "status" => "success",
    "country" => $record->country->isoCode,
    "country_name" => $record->country->name,
    "ip" => htmlspecialchars($_GET["ip"]),
  );
} catch (\MaxMind\Db\InvalidDatabaseException $e){
  $return = array(
    "status" => "error",
    "error" => "Invalid Database",
  );
  http_response_code(500);
} catch (\GeoIp2\Exception\AddressNotFoundException $e){
  $return = array(
    "status" => "error",
    "error" => "Address not found",
  );
  http_response_code(404);
} catch (InvalidArgumentException $e){
  $return = array(
    "status" => "error",
    "error" => "Invalid IP address",
  );
  http_response_code(400);
}

header("Content-Type: application/json");
echo json_encode($return);
