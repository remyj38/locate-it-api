# Locate it API

[![pipeline status](https://gitlab.com/remyj38/locate-it-api/badges/master/pipeline.svg)](https://gitlab.com/remyj38/locate-it-api/commits/master)

This code is used by the [Locate it Browser Addon](https://gitlab.com/remyj38/locate-it) to localize an IP.

## Requirements

- PHP
- Composer

## Installation

### From console

In your web root directory:
```bash
git clone https://gitlab.com/remyj38/locate-it-api.git
cd locate-it-api
composer install
# Add a cron job to update GeoLite database each month on the 7th
(crontab -l 2>/dev/null; echo "0 0 7 * * $(which php) $(pwd)/update-db.php") | crontab -
```

You can now use the API on http://your_host/locate-it-api/

### With FTP and Browser

1. Download the last build on https://gitlab.com/remyj38/locate-it-api/-/jobs/artifacts/master/download?job=release
2. Extract it on your web root directory
3. Go to http://your_host/locate-it-api/update-db.php to download the GeoLite Database.
4. If your host provider allow it, define a scheduled job each month on the 7th (at the time you want). The job should execute the update-db.php script (`php update-db.php`). Whithout this step, you will need to update manually the database (released on the first Tuesday of each month).
