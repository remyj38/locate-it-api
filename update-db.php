#!/usr/bin/php
<?php

define("DOWNLOAD_LINK", "http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz");

/**
 * Recursively delete a directory
 *
 * @param string $dir Target directory
 * @param string $keepDir Optionnal Keep the current directory
 *
 * @return boolean
 */
function recursiveDelete($dir, $keepDir = false) {
  $files = array_diff(scandir($dir), array('.','..'));
  foreach ($files as $file) {
    if ($file !== ".gitkeep"){
      (is_dir("$dir/$file")) ? recursiveDelete("$dir/$file") : unlink("$dir/$file");
    }
  }
  if ($keepDir){
    return true;
  }
  return rmdir($dir);
}

echo "\033[32mCleaning database folder...\033[0m\n";
if (recursiveDelete('database', true)) {
  echo "\033[32mDatabase folder cleaned !\033[0m\n";
} else {
  echo "\033[33mFail to clean database folder !\033[0m\n";
}

echo "\033[32mDownloading GeoIP database...\033[0m\n";
// Creating temporary file
$downloadLocation = tempnam(sys_get_temp_dir(), "");
rename($downloadLocation, $downloadLocation .= '.tar.gz');

file_put_contents($downloadLocation, file_get_contents(DOWNLOAD_LINK));
// Checking file sum
$fileMD5 = file_get_contents(DOWNLOAD_LINK . ".md5");
if (md5_file($downloadLocation) !== $fileMD5) {
  echo "\033[31mDownloaded file sum is incorrect !\033[0m\n";
  unlink($downloadLocation);
  exit(1);
}
echo "\033[32mGeoIP database downloaded !\033[0m\n";

echo "\033[32mExtracting database...\033[0m\n";
try {
  $archiveGZ = new PharData($downloadLocation);
  $archiveGZ->decompress(); // Extract to .tar archive
  $archive = new PharData(substr($downloadLocation, 0, -3));
  foreach ($archive as $file){ // Get Subfolder name
    if ($file->isDir() && preg_match("/GeoLite2-City_\d{8}/", $file->getFilename())){
      $folder = $file->getFilename();
      continue;
    }
  }

  if (!isset($folder)){
    echo "\033[31mCan't find database folder in archive !\033[0m\n";
    exit(1);
  }

  $archive->extractTo("./database/", "$folder/GeoLite2-City.mmdb");
  rename("./database/$folder/GeoLite2-City.mmdb", "./database/GeoLite2-City.mmdb");
  chmod("./database/GeoLite2-City.mmdb", 0755);
  rmdir("./database/" . $folder);
} catch (Exception $e) {
  print_r($e->getMessage());
  echo "\033[31mFailed to extract file !\033[0m\n";
  unlink($downloadLocation);
  unlink(substr($downloadLocation, 0, -3));
  exit(1);
}
echo "\033[32mDatabase extracted !\033[0m\n";

echo "\033[32mCleaning temporary files...\033[0m\n";
unlink($downloadLocation);
unlink(substr($downloadLocation, 0, -3));
echo "\033[32mTemporary files cleaned !\033[0m\n";

echo "\n\n\033[32mDone !\033[0m\n";
